package com.example.ejemplosqlite;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.service.autofill.TextValueSanitizer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ejemplosqlite.utilidades.utilidades;

public class registro_activity extends AppCompatActivity {
    TextView campoId, campoNombre, campoTelefono;
    Button btnRegistrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_activity);



        campoId = (TextView) findViewById(R.id.editTextId);
        campoNombre = (TextView) findViewById(R.id.editTextNombre);
        campoTelefono = (TextView) findViewById(R.id.editTextTelefono);
        btnRegistrar = (Button)findViewById(R.id.btnRegistrarUsuario);
        setupActionBAr();
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarUsuarios();
            }
        });
    }



    private void registrarUsuarios() {
        ConexionSQLiteHelper conn= new ConexionSQLiteHelper(this, "bd_usuarios",null,1);

        SQLiteDatabase db=conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(utilidades.CAMPO_ID, campoId.getText().toString());
        values.put(utilidades.CAMPO_NOMBRE, campoNombre.getText().toString());
        values.put(utilidades.CAMPO_TELEFONO, campoTelefono.getText().toString());

        Long idResultante=db.insert(utilidades.TABLA_USUARIO,utilidades.CAMPO_ID,values);

        Toast.makeText(getApplicationContext(),"Id Registro: "+idResultante,Toast.LENGTH_SHORT).show();
        db.close();

    }
    private void setupActionBAr() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }
}
