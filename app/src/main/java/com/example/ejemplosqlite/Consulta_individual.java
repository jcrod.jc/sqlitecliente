package com.example.ejemplosqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ejemplosqlite.utilidades.utilidades;

public class Consulta_individual extends AppCompatActivity {
    TextView campoId,campoNombre,campoTelefono;
    Button botonBuscar,botonEliminar,botonActualizar;
    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_individual);

         conn= new ConexionSQLiteHelper(this, "bd_usuarios",null,1);


        campoId =(TextView)findViewById(R.id.editTextIdBuscar);
        campoNombre =(TextView)findViewById(R.id.editTextNombreBuscar);
        campoTelefono =(TextView)findViewById(R.id.editTextTelefonoBuscar);

        botonBuscar = (Button)findViewById(R.id.btnBuscar);
        botonActualizar = (Button)findViewById(R.id.btnActualizar);
        botonEliminar = (Button)findViewById(R.id.btnEliminar);

        botonActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Actualizar();

            }

            private void Actualizar() {
                SQLiteDatabase db=conn.getWritableDatabase();
                String[] parametros = {campoId.getText().toString()};
                ContentValues values = new ContentValues();
                values.put(utilidades.CAMPO_NOMBRE,campoNombre.getText().toString());
                values.put(utilidades.CAMPO_TELEFONO,campoTelefono.getText().toString());

                db.update(utilidades.TABLA_USUARIO,values,utilidades.CAMPO_ID+"=?",parametros);
                Toast.makeText(getApplicationContext(),"Ya se Actualizaron los datos",Toast.LENGTH_LONG).show();
                Limpiar();
                db.close();

            }
            private void Limpiar() {
                campoNombre.setText("");
                campoTelefono.setText("");
            }


        });
        botonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Eliminar();

            }

            private void Eliminar() {
                SQLiteDatabase db=conn.getWritableDatabase();
                String[] parametros = {campoId.getText().toString()};

                db.delete(utilidades.TABLA_USUARIO,utilidades.CAMPO_ID+"=?",parametros);
                Toast.makeText(getApplicationContext(),"Se elimino el Usuario",Toast.LENGTH_LONG).show();
                Limpiar();
                db.close();




            }
            private void Limpiar() {
                campoNombre.setText("");
                campoTelefono.setText("");
            }
        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            consultar();
            }

            private void consultar() {

                SQLiteDatabase db=conn.getReadableDatabase();
                String[] parametros={campoId.getText().toString()};
                String[] campos= {utilidades.CAMPO_NOMBRE,utilidades.CAMPO_TELEFONO};

                try{
                    Cursor cursor = db.query(utilidades.TABLA_USUARIO,campos, utilidades.CAMPO_ID+"=?",parametros,null,null,null);
                    cursor.moveToFirst();

                    campoNombre.setText(cursor.getString(0));
                    campoTelefono.setText(cursor.getString(1));
                    cursor.close();

                }catch (Exception e){

                    Toast.makeText(getApplicationContext(),"El documento no existe",Toast.LENGTH_LONG).show();
                    Limpiar();



                }



            }

            private void Limpiar() {
                campoNombre.setText("");
                campoTelefono.setText("");
            }
        });

    }
}
